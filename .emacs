(add-hook 'awk-mode-hook
          '(lambda ()
           (font-lock-mode nil)
             ))

(add-hook 'f90-mode-hook
          '(lambda ()
           (font-lock-mode nil)
             ))

(when (file-exists-p "/usr/local/bin/aspell")
  (setq-default ispell-program-name "/usr/local/bin/aspell")
  (setq-default ispell-extra-args '("--reverse"))
)

(when (display-graphic-p)
  (tool-bar-mode -1)
  (when (member "Consolas" (font-family-list))
    (set-default-font "Consolas-14:weight=normal" nil nil)
    )
  )

;;;;;;;;;;;;;;;;;;;;;;;
;;; General setting ;;;
;;;;;;;;;;;;;;;;;;;;;;;
(define-key function-key-map [delete] [F20])
(global-set-key [F20] 'delete-char)
(global-linum-mode t)
(column-number-mode t)
(show-paren-mode t)

(setq linum-format "%3d\u2502")

(setq tex-default-mode    'latex-mode)
;(setq latex-run-command   "rtex")

(add-hook 'latex-mode
          '(lambda ()
           (flyspell-mode nil)
             ))

(global-hl-line-mode 1)
(set-face-attribute hl-line-face nil :background "#102077")
(set-face-attribute hl-line-face nil :foreground "White")

(defun jao-selective-display ()
"Activate selective display based on the column at point"
(interactive)
(set-selective-display
(if selective-display
nil
(+ 1 (current-column)))))
(global-set-key [f1] 'jao-selective-display)

(setq latex-run-command   "latex")
(add-hook 'tex-mode-hook '(lambda () (setq tex-start-options-string nil)))
        ; stop at each tex (or latex) error


(when (executable-find "yap.exe")
  (setq tex-dvi-view-command    "yap.exe")
)

(defun set-pdflatex nil "Set default latex to pdflatex, and dvi viewer to gv"
  (interactive)
  (setq latex-run-command   "pdflatex")
  (when (executable-find "gv")
    (setq tex-dvi-view-command    "gv")
    )
  )

 ;;(set-frame-parameter (selected-frame) 'alpha '(<active> [<inactive>]))
(set-frame-parameter (selected-frame) 'alpha '(85 50))
(add-to-list 'default-frame-alist '(alpha 85 50))

(eval-when-compile (require 'cl))

(defun toggle-transparency ()
  (interactive)
  (if (/=
       (cadr (frame-parameter nil 'alpha))
       100)
      (set-frame-parameter nil 'alpha '(100 100))
    (set-frame-parameter nil 'alpha '(85 50))))
(global-set-key (kbd "C-c t") 'toggle-transparency)


(setq default-major-mode 'text-mode)          ; Default mode for unknown file
(setq default-tab-width 4)
(setq indent-tabs-mode nil)
(setq c-basic-offset 4)
(setq c++-basic-offset 4)
(setq-default indent-tabs-mode nil)

;(set-frame-height (selected-frame) 42)
;(standard-display-european     t)             ; Enable display of accent



(add-hook 'comint-output-filter-functions
    'shell-strip-ctrl-m nil t)
(add-hook 'comint-output-filter-functions
    'comint-watch-for-password-prompt nil t)
(when (executable-find "bash.exe")
  (setq explicit-shell-file-name "bash.exe")
  ;; For subprocesses invoked via the shell
  ;; (e.g., "shell -c command")
  (setq shell-file-name explicit-shell-file-name)
  )

(defun re-conf ()
  "Ricarica il file di configurazione"
  (interactive)
  (load-file "$HOME/.emacs")
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Global keyboard setting ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key [f1]   'bmm-remove-template)
(global-set-key [?\C-u]  'bmm-justify)
;(global-set-key [?\C-i]  'bmm-fill)
(global-set-key [f2]   'bmm-remove-namespaces)
;(global-set-key    [f2]   'kill-this-buffer)
;(global-set-key [f3]   'make-frame)
(global-set-key [f3]   'set-bmm-bookmark)
(global-set-key [S-f3] 'goto-bmm-bookmark)
(global-set-key [f4]   'undo)
(global-set-key [f5]   'compile)
(global-set-key [f8]   'describe-function)
;;(global-set-key [f9]   'describe-variable)
(when (>= emacs-major-version 24)
  (global-set-key [f9] 'whitespace-mode))
(global-set-key [f10]  're-conf)
(global-set-key [S-f10]  'bmm-large-font)
(global-set-key [S-f7] 'load-font)
;(global-set-key [C-tab]  'bmm-delete-word)
(global-set-key [(control x) (control l)] 'windmove-right)
(global-set-key [(control x) (control j)] 'windmove-left)
(global-set-key [(control x) (control k)] 'windmove-down)
(global-set-key [(control x) (control i)] 'windmove-up)
(global-set-key [(control x) (control m)]  'set-mark)


;(load-file "/usr/share/emacs/site-lisp/mwheel.elc")
;(load-library "iso-insert")
;(define-key global-map [?\C-.] 8859-1-map)

(autoload 'wikipedia-mode "wikipedia-mode.el"
"Major mode for editing documents in Wikipedia markup." t)

(autoload 'matlab-mode "matlab-mode" "Enter Matlab mode." t)
(setq auto-mode-alist (cons '("\\.m$" . matlab-mode) auto-mode-alist))
(defun my-matlab-mode-hook ()
  (setq fill-column 76)
  (turn-on-auto-fill))
(setq matlab-mode-hook 'my-matlab-mode-hook)

;;;;;;;;;;;;;;;;;
;;; MAKE Mode ;;;
;;;;;;;;;;;;;;;;;

(autoload 'makefile-mode "makefile-mode" "makefile major mode." t)
(or (assoc "Makefile" auto-mode-alist)
    (setq auto-mode-alist (cons '("Makefile" . makefile-mode)
                auto-mode-alist))
    )
(add-hook 'makefile-mode-hook
      '(lambda ()
         (font-lock-mode)
         ))

;;;;;;;;;;;;;;;;;
;;; Html Mode ;;;
;;;;;;;;;;;;;;;;;

;; (autoload 'html-mode "html-mode" "HTML major mode." t)
;; (or (assoc "\\.html$" auto-mode-alist)
;;      (setq auto-mode-alist (cons '("\\.html$" . html-mode)
;;                      auto-mode-alist))
;;      )

;; (add-hook 'html-mode-hook
;;            '(lambda ()
;;               (cond (window-system
;;                  (set-face-foreground 'italic    "aquamarine")
;;                  (set-face-foreground 'underline "green")
;;                  ))
;;               ))



;;;;;;;;;;;;;;;;;;;;
;;; C & C++ Mode ;;;
;;;;;;;;;;;;;;;;;;;;
;; (eval-after-load "cc-mode"
;;   '(progn
;;      (defun use-rtags (&optional useFileManager)
;;        (and (rtags-executable-find "rc")
;;             (cond ((not (gtags-get-rootpath)) t)
;;                   ((and (not (eq major-mode 'c++-mode))
;;                         (not (eq major-mode 'c-mode))) (rtags-has-filemanager))
;;                   (useFileManager (rtags-has-filemanager))
;;                   (t (rtags-is-indexed)))))

;;      (defun tags-find-symbol-at-point (&optional prefix)
;;        (interactive "P")
;;        (if (and (not (rtags-find-symbol-at-point prefix)) rtags-last-request-not-indexed)
;;            (gtags-find-tag)))
;;      (defun tags-find-references-at-point (&optional prefix)
;;        (interactive "P")
;;        (if (and (not (rtags-find-references-at-point prefix)) rtags-last-request-not-indexed)
;;            (gtags-find-rtag)))
;;      (defun tags-find-symbol ()
;;        (interactive)
;;        (call-interactively (if (use-rtags) 'rtags-find-symbol 'gtags-find-symbol)))
;;      (defun tags-find-references ()
;;        (interactive)
;;        (call-interactively (if (use-rtags) 'rtags-find-references 'gtags-find-rtag)))
;;      (defun tags-find-file ()
;;        (interactive)
;;        (call-interactively (if (use-rtags t) 'rtags-find-file 'gtags-find-file)))
;;      (defun tags-imenu ()
;;        (interactive)
;;        (call-interactively (if (use-rtags t) 'rtags-imenu 'idomenu)))
;;      )

;;   (define-key c-mode-base-map (kbd "M-.") (function tags-find-symbol-at-point))
;;   (define-key c-mode-base-map (kbd "M-,") (function tags-find-references-at-point))
;;   (define-key c-mode-base-map (kbd "M-;") (function tags-find-file))
;;   (define-key c-mode-base-map (kbd "C-.") (function tags-find-symbol))
;;   (define-key c-mode-base-map (kbd "C-,") (function tags-find-references))
;;   (define-key c-mode-base-map (kbd "C-<") (function rtags-find-virtuals-at-point))
;;   (define-key c-mode-base-map (kbd "M-i") (function tags-imenu))

;;   (define-key global-map (kbd "M-.") (function tags-find-symbol-at-point))
;;   (define-key global-map (kbd "M-,") (function tags-find-references-at-point))
;;   (define-key global-map (kbd "M-;") (function tags-find-file))
;;   (define-key global-map (kbd "C-.") (function tags-find-symbol))
;;   (define-key global-map (kbd "C-,") (function tags-find-references))
;;   (define-key global-map (kbd "C-<") (function rtags-find-virtuals-at-point))
;;   (define-key global-map (kbd "M-i") (function tags-imenu))
;; )

(add-hook 'c++-mode-hook '(lambda () (add-hook 'before-save-hook 'delete-trailing-whitespace)))

;(fmakunbound 'c-mode)
;(makunbound 'c-mode-map)
;(fmakunbound 'c++-mode)
;(makunbound 'c++-mode-map)
;(makunbound 'c-style-alist)

(setq auto-mode-alist
      (append
       '(("\\.C$"  . c++-mode)
         ("\\.H$"  . c++-mode)
         ("\\.cc$" . c++-mode)
         ("\\.hh$" . c++-mode)
         ("\\.hpp$" . c++-mode)
         ("\\.cu$" . c++-mode)
          ("\\.c$"  . c-mode)
         ("\\.h$"  . c++-mode)
         ) auto-mode-alist)
      )

(add-hook 'c-mode-common-hook
          '(lambda ()
;;           (c-toggle-auto-hungry-mode t)
             (setq enable-local-eval t)
             (font-lock-mode t)
             (define-key c-mode-map [f5] 'compile)
             (define-key c-mode-map [f6] 'next-error)
             ))


;;;;;;;;;;;;;;;;;;;;;;;;
;;; TeX & LaTeX Mode ;;;
;;;;;;;;;;;;;;;;;;;;;;;;

(defun unfold-template nil "takes an error message with templates and indent it to make it readable."
  (interactive)
  (progn
    (goto-char 0)
    (replace-string "<" "<
")
    (goto-char 0)
    (replace-string "," ",
")
    (goto-char 0)
    (replace-string ">" "
>")
    (mark-whole-buffer)
    (indent-region (region-beginning) (region-end) nil)
    )
  )

(defun unfold-preprocessed nil "takes an error message with templates and indent it to make it readable."
  (interactive)
  (progn
    (goto-char 0)
    (replace-string "{" "{
")
    (goto-char 0)
    (replace-string "; " ";
")
    (goto-char 0)
    (replace-string "}" "}
")
    (mark-whole-buffer)
    (indent-region (region-beginning) (region-end) nil)
    )
  )


(defun bmm-parentesi nil "" (interactive)
  (progn
    (insert "{}")
    (backward-char 1)
    ))

(add-hook 'tex-mode-hook
      '(lambda ()
         (font-lock-mode nil)
         (define-key tex-mode-map [f5] 'validate-tex-buffer)
         (define-key tex-mode-map [f6] 'tex-file)
         (define-key tex-mode-map [f7] 'tex-view)
         (define-key tex-mode-map [f8] 'fill-paragraph)
;        (define-key tex-mode-map [f4] 'bmm-parentesi)
         (define-key tex-mode-map [?\C-p] 'bmm-parentesi)
         ))

(add-hook 'tex-mode-hook 'auto-fill-mode)
;(add-hook 'tex-mode-hook 'iso-accents-mode)

;;;;;;;;;;;;;;;;;;;;
;;; Folding Mode ;;;
;;;;;;;;;;;;;;;;;;;;

(autoload 'folding-mode "folding"
  "Minor mode that simulates a folding editor" t)

(defun folding-mode-find-file-hook ()
  "One of the hooks called whenever a `find-file' is successful."
  (and (assq 'folded-file (buffer-local-variables))
       folded-file
       (folding-mode 1)
       (kill-local-variable 'folded-file)))

(or (memq 'folding-mode-find-file-hook find-file-hooks)
    (setq find-file-hooks (append find-file-hooks
                  '(folding-mode-find-file-hook))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Some useful extensions ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq ver (substring (version) 10 16))

(defun bmm-justify-all nil "Giustifica il buffer"
  (interactive)
  (progn
    (mark-whole-buffer)
    (fill-region (region-beginning) (region-end) (quote full))
    ))

(defun bmm-justify nil "Giustifica il paragrafo"
  (interactive)
  (progn
    (fill-paragraph (quote full))
    ))

(defun bmm-fill nil "Riempie il paragrafo"
  (interactive)
  (progn
    (fill-paragraph nil)
    ))

(defun bmm-togli-spazi-alla-fine-di-ogni-riga nil "indovina"
  (interactive)
  (progn
    (goto-char 0)
    (replace-regexp " *$" "" nil)
    ))

(defun bmm-show-version nil "Mostra la variabile ver settata da bmm"
  (interactive)
  (print ver))

(defun bmm-delete-word nil "Cancella la parola in cui � il cursore o la successiva se il cursore non � su una parola."
  (interactive)
  (progn
    (forward-word 1)
    (backward-word 1)
    (mark-word 1)
    (delete-region (region-beginning) (region-end))
    ))


(defun bmm-sostituiscitutti (fs ts) "Sostituisce una stringa in tutti i buffer aperti"
  (interactive "sStringa da cercare: \nsSostituto: ")
  (progn
    (defvar lista nil "")
    (defvar n nil "")
    (setq lista (buffer-list))
    (setq n (length lista))
    (while (> n 0)
      (progn
        (print (buffer-name (nth n lista)) (get-buffer "*scratch*"))
        (set-buffer (buffer-name (nth n lista)))
        (setq n (- n 1))
        (print (buffer-name (nth n lista)) (get-buffer "*scratch*"))
        (if (and (equal buffer-read-only nil) (> (buffer-size) 0))
            (progn
              (goto-char 0)
              (replace-string fs ts)
              )
          )
        )
      )
    )
  )

(defun bmm-trova-qui (ss) "Tova una stringa nel buffer corrente"
  (interactive "sEspressione regolare da cercare: ")
  (defvar nome nil "")
  (defvar pippo nil "")
  (defvar poppi nil "")
  (progn
    (setq poppi (point))
    (setq nome (buffer-name (current-buffer)))
    (print nome (get-buffer "*scratch*"))
    (set-buffer (get-buffer nome))
    (if (> (buffer-size) 0)
        (progn
          (goto-char (point-min))
          (while (re-search-forward ss (point-max) t)
            (progn
              (setq pippo (point))
;             (print (point) (get-buffer "*scratch*"))
;             (set-buffer (get-buffer nome))
              (beginning-of-line)
              (push-mark (point) t t)
              (end-of-line)
;             (print (point) (get-buffer "*scratch*"))
;             (set-buffer (get-buffer nome))
;             (print (mark) (get-buffer "*scratch*"))
;             (set-buffer (get-buffer nome))
;             (print pippo (get-buffer "*scratch*"))
;             (set-buffer (get-buffer nome))
              (copy-to-register 100 (point) (mark))
              (print (get-register 100) (get-buffer "*scratch*"))
              (pop-mark)
              )
            )
          )
      )
    )
  (goto-char poppi)
  )


(defun bmm-trova-in-tutti (fs) "Tova una stringa in tutti i buffer aperti"
  (interactive "sEspressione regolare da cercare: ")
  (progn
    (defvar lista nil "")
    (defvar n nil "")
    (defvar pippo nil "")
    (setq lista (buffer-list))
    (setq n (length lista))
    (while (> n 0)
      (progn
        (set-buffer (buffer-name (nth n lista)))
        (if (not (string-match "*" (buffer-name (current-buffer))))
            (bmm-trova-qui fs)
          )
        (setq n (- n 1))
        )
      )
    )
  )

(defun bmm-remove-namespaces nil "removes gridtools:: boost:: fusion:: and mpl:: namespaces from text"
  (interactive)
  (push-mark (beginning-of-line) t t)
  (end-of-line)

  (replace-string "gridtools::" "" nil (region-beginning) (region-end))
  (replace-string "boost::" "" nil (region-beginning) (region-end))
)

(defun bmm-remove-template nil "removes the largest template starting at current position in the buffer"
  (interactive)
  (push-mark (point) t t)
  (setq mystack '())
  (setq keepgoing t)
  (setq donothing t)
  (progn
    (while keepgoing
      (progn
        (if (string= (string (char-after)) "<")
              (setq mystack (cons 1 mystack))
          (if (string= (string (char-after)) ">") ; else
              (if (null mystack)
                  (setq keepgoing nil)
                (progn
                  (setq mystack (cdr mystack))
                  (if (null mystack)
                      (progn
                        (setq donothing nil)
                        (setq keepgoing nil)
                        )
                    ) ; if no else
                  ) ; progn else
                ) ; else
            )
          )

        (forward-char)

        ) ; progn
      ) ; while
    (if donothing (progn (deactivate-mark) (message "no template found"))
      (progn
        (delete-region (region-beginning) (region-end))
        (insert " ... ")
        )
      )
    )
  )


(defvar n 0 "items counter")
(defvar lista nil "")
(setq lista (buffer-list))

(defun toglispazi nil "MA"
  (interactive)
  (progn
    (setq lista (buffer-list))
    (setq n 0)
;    (setq stringa-comando (read-string "COMANDO: "))
;    (setq comando (list (substring stringa-comando 0 (length stringa-comando))))
    (while (not (equal (nth n lista) nil))
      (progn
    (set-buffer (buffer-name (nth n lista)))
    (if (and (equal buffer-read-only nil) (> (buffer-size) 0))
        (progn
          (goto-char 0)
;         (eval comando)
          (replace-string "write" "!scrivi" nil)
;         (goto-char 0)
;         (replace-string "WRITE" "!SCRIVI" nil)
          (print (buffer-name (nth n lista)) (get-buffer "*scratch*"))
          )
      )
    (setq n (+ n 1))
    )
      )
    )
  )

(defun a-capo-dopo-punto-e-virgola nil "MA"
  (interactive)
  (progn
    (goto-char 0)
    (while (re-search-forward ";")
          (newline)
          )
    )
  )


(defvar bmm-bookmark 0 "Che ca**o te ne frega di che ca**o e` questo?")
(defun set-bmm-bookmark nil "mette un segnalibricino"
  (interactive)
  (progn
    (set-variable 'bmm-bookmark (point))
    ))

(defun goto-bmm-bookmark nil "va al segnalibricino"
  (interactive)
  (progn
    (goto-char bmm-bookmark)
    ))


(defun unix2dos nil "unix2dossizzazione del buffer corrente"
  (interactive)
  (progn
    (goto-char 0)
    (replace-regexp "$" "")))

(defun dos2unix nil "dos2unixizzazione del buffer corrente"
  (interactive)
  (progn
    (goto-char 0)
    (replace-regexp "$" "")))

(defun bmm-large-font nil "Impone font 10x20 se in grafica"
  (interactive)
  (cond (window-system
     (progn (set-default-font "10x20")
            (set-frame-height (selected-frame) 36)
            ))))



(defun bmm-permute-names (iter) "For stapl meeting"
  (interactive "nPregenerate number: ")
  (setq names (list 'Antal 'Dylan 'Gabi 'Harsh 'Hector 'Ioannis 'Jeremy 'Nathan 'Rudrajit 'Tarun 'Timmie 'Sam 'Shuai 'Xiabing))
  (setq leng (length names))

  ;(setq dst names)
  (setq n 0)
  (while (< n iter)
    (setq rnd (random 100))
    (setq n (1+ n))
    )

  (setq dst names)
  (setq nn 0)
  (while (< nn (* 2 leng))
    (setq src dst)
    (setq dst nil)
    (setq m 0)

    (while (< m leng)
      (setq rnd (random 100))
      ;(princ (format "rnd %d\n" rnd))
      (cond
       ((>= rnd 50)
        (setq dst (nconc dst (list (car src))))
        (setq src (cdr src)))
       ((< rnd 50)
        (setq dst (cons (car src) dst))
        (setq src (cdr src)))
       )
      ;(princ dst)
      (setq m (1+ m))
      ;(princ (format "\n------ %d\n" m))
      )
    (setq nn (1+ nn))
    ;(princ (format "mah %d\n" nn))
    )

  (setq nn 0)
  (while (< nn leng)
    (princ (format "- (%s)\n\n" (car dst)) (current-buffer))
    (setq dst (cdr dst))
    (setq nn (1+ nn))
    )
  )

; funzione ricorsiva che calcola il fattoriale di un numero.
; La funzione e` interattiva!
(defun Fattoriale (n) "Calcola il fattoriale"
  (interactive "nIntero n : ")
  (if (and (integerp n) (>= n 0))
      (if (> n 1)
      (* n (Fattoriale (- n 1)))
    (if (= n 0) (+ n 1) (+ n 0)))
    (if (not (integerp n))
    (error "Wrong argument type: argomento non intero")
      (if (< n 0)
      (error "Argument less that 0")
    (error "Some other kind of error")))
    )
  )

(load-library "paren")

(load "completion")
(initialize-completions)

; Date and time
(display-time)
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)

(line-number-mode t)

;;;;;;;;;;;;;;;;;;;;;;
;;; Colors & Faces ;;;
;;;;;;;;;;;;;;;;;;;;;;
;   (progn
      ;; (set-scroll-bar-mode (quote right) (quote right))
      ;; (set-scroll-bar-mode (quote right)) )
      ;(setq x-pointer-shape x-pointer-left-ptr)
      (set-mouse-color                          "cyan")
      (set-cursor-color                         "yellow")
      (when window-system    (set-background-color "#101219"))
      (set-foreground-color                     "antique white")
;     (set-face-background 'modeline            "blue")
;     (set-face-foreground 'modeline            "yellow")
      (set-face-background 'highlight           "red")
;     (set-face-background 'highlight           "PeachPuff")
      (set-face-foreground 'highlight           "yellow")
      (set-face-background 'region              "gray")
      (set-face-foreground 'region              "red")
      (set-face-background 'secondary-selection "gray")
      (set-face-foreground 'secondary-selection "black")
      (set-face-foreground 'minibuffer-prompt   "yellow")
;     (set-face-background 'bold                "slate gray")
      (set-face-foreground 'bold                "green")
;     (set-face-background 'italic              "midnight blue")
      (set-face-foreground 'italic              "green")
;     (set-face-background 'bold-italic         "midnight blue")
      (set-face-foreground 'bold-italic         "yellow")
;     (set-face-background 'underline           "black")
      (set-face-foreground 'underline           "gold")

  (progn
    (set-cursor-color "white"))

(setq default-frame-alist
      (append '((background-color . "black")
                (foreground-color . "antique white")
                (cursor-color     . "yellow")
                (mouse-color      . "cyan")
                (cursor-type      . block) ; bar
                ) default-frame-alist))


;(custom-set-variables
; '(latex-run-command "latex master.tex"))

;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(ansi-color-names-vector ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
;;  '(custom-enabled-themes (quote (deeper-blue)))
;;  '(inhibit-startup-screen t))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  )

(put 'upcase-region 'disabled nil)


(put 'downcase-region 'disabled nil)

(message ".emacs done") ; messaggio di fine configurazione
